data "google_compute_network" "default" {
  name = "default"
}

data "google_compute_subnetwork" "default" {
  name   = "default"
  region = "europe-west2"
}

locals {
  project = "alert-flames-314805"
  labels = {
    purpose = "web-tier",
  }
  # web tier ILB variables  
  ilb1_web_tier_region = "europe-west2"
  ilb1_web_tier_name   = "web_ilb"
  ilb1_web_tier_named_ports = [{
    name = "http"
    port = 80
  }]
  ilb1_web_tier_health_check = {
    type                = "http"
    check_interval_sec  = 1
    healthy_threshold   = 4
    timeout_sec         = 1
    unhealthy_threshold = 5
    response            = ""
    proxy_header        = "NONE"
    port                = 80
    port_name           = "health-check-port"
    request             = ""
    request_path        = "/"
    host                = local.ilb1_web_tier_ip_address
    enable_log          = false
  }
  ilb1_web_tier_ip_address = "1.2.3.4"

  # web tier MIG variables 
  mig1_web_tier_region          = "europe-west2"
  mig1_web_tier_name            = "web_mig"
  mig1_web_tier_service_account = "mig1_web_tier@${local.project}.iam.gserviceaccount.com"


  # app tier ILB variables 
  ilb1_app_tier_region = "europe-west2"
  ilb1_app_tier_name   = "app_ilb"
  ilb1_app_tier_named_ports = [{
    name = "https"
    port = 443
  }]
  ilb1_app_tier_health_check = {
    type                = "http"
    check_interval_sec  = 1
    healthy_threshold   = 4
    timeout_sec         = 1
    unhealthy_threshold = 5
    response            = ""
    proxy_header        = "NONE"
    port                = 443
    port_name           = "health-check-port"
    request             = ""
    request_path        = "/"
    host                = local.ilb1_app_tier_ip_address
    enable_log          = false
  }
  ilb1_app_tier_ip_address = "1.2.3.5"

  mig1_app_tier_region          = "europe-west2"
  mig1_app_tier_name            = "app_mig"
  mig1_app_tier_service_account = "mig1_app_tier@${local.project}.iam.gserviceaccount.com"

  db_name = "testing"
}


