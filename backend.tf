terraform {
  backend "http" {
  }
}

provider "google" {
  version     = "~> 4.26"
  project     = local.project
  credentials = var.terraform_viewer_key
}

variable "terraform_viewer_key" {}
