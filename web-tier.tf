data "template_file" "web_tier_startup_script" {
  template = file("${path.root}/web-tier-startup.sh.tpl")
  vars     = {}
}


data "google_compute_zones" "available" {
  region = local.mig1_web_tier_region
}

module "mig1_web_tier_vm_instance_template" {
  source  = "terraform-google-modules/vm/google//modules/instance_template"
  version = "7.8.0"
  service_account = {
    email  = local.mig1_app_tier_service_account,
    scopes = ["cloud-platform"]
  }

  startup_script = data.template_file.web_tier_startup_script.rendered
  network        = data.google_compute_network.default.name
  subnetwork     = data.google_compute_subnetwork.default.name
  region         = local.mig1_web_tier_region
  name_prefix    = local.mig1_web_tier_name
  labels         = local.labels
}

module "mig1_web_tier" {
  source                    = "terraform-google-modules/vm/google//modules/mig"
  version                   = "7.8.0"
  autoscaling_mode          = "OFF"
  instance_template         = module.mig1_web_tier_vm_instance_template.self_link
  project_id                = local.project
  region                    = local.mig1_web_tier_region
  distribution_policy_zones = data.google_compute_zones.available.names
  mig_name                  = local.mig1_web_tier_name
  target_size               = 3
  network                   = data.google_compute_network.default.name
  subnetwork                = data.google_compute_subnetwork.default.name
  update_policy = [{
    type                           = "PROACTIVE"
    instance_redistribution_type   = "PROACTIVE"
    minimal_action                 = "REPLACE"
    most_disruptive_allowed_action = "REPLACE"
    max_surge_fixed                = 2
    max_unavailable_fixed          = 1
    min_ready_sec                  = 50
    replacement_method             = "RECREATE"
    max_surge_percent              = null
    max_unavailable_percent        = null
  }]
}
