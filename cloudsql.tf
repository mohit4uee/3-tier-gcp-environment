module "sql-db_example_postgresql-ha" {
  source     = "GoogleCloudPlatform/sql-db/google//examples/postgresql-ha"
  version    = "11.0.0"
  project_id = local.project
  pg_ha_name = local.db_name
}
