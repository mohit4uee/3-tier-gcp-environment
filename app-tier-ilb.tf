module "ilb1_app_tier" {
  source       = "GoogleCloudPlatform/lb-internal/google"
  version      = "5.0.0"
  project      = local.project
  region       = local.ilb1_app_tier_region
  name         = "app-ilb"
  ports        = [local.ilb1_app_tier_named_ports[0].port]
  source_tags  = ["allow-group1"]
  target_tags  = ["allow-group2"]
  health_check = local.ilb1_app_tier_health_check
  ip_address   = local.ilb1_app_tier_ip_address

  backends = [
    {
      group       = module.mig1_app_tier.instance_group
      description = "app Tier MIG"
      failover    = false
    },
  ]
}
