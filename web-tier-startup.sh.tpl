#!/bin/bash

apt-get update
apt-get install -y apache2 libapache2-mod-php

sudo mv /var/www/html/index.html /var/www/html/index.html.old

cat > /var/www/html/index.html <<'EOF'
<!doctype html>
<html>
<head>
<p>Welcome, this is your web-tier :)<p>
</head>

EOF

systemctl enable apache2
systemctl restart apache2

