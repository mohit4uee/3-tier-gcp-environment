# 3-tier GCP Environment

This repo is to setup 3-tier environment on GCP using terraform.
![alt text](files/3-tier-env.jpg)

## Assumptions:

1.	GCP project is already created.
2.	Network already created and VM’s compute SA have access to use the network
3.	The user calling this module has updated teh local variable as desired
4.	Require firewalls are already opened to allow communication
5.	All workloads, web tier & app tier are hosted on VMs.
6.	CloudSQL instance  (Postgre) is used as DB
7.  User accessing the web-tier ILB, has network connectivity as desired.


## Control requirement:

User consuming this module shall have permissions to create following resources in specific project:
1. 	VM
2.	ILB
3.	Healthcheck
4.  Cloudsql

